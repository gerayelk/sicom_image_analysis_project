import numpy as np
from .clip_values import clip_values
from .process_channel import process_channel

def process_green_channel(img: np.ndarray, N: int, M: int) -> np.ndarray:
    """Process the green channel of an image
    
    Args:
        img (np.ndarray): image to process
        N (int): height of the image
        M (int): width of the image
        
    Returns:
        np.ndarray: processed green channel of the image
    """
    
    img = process_channel(img, 1, N, M)                
    img = clip_values(img)
    
    return img