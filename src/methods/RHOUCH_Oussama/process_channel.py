import numpy as np
from .find_neighbors import find_neighbors
from .find_direct_neighbors import find_direct_neighbors
from .find_weights import find_weights
from .interpolate import interpolate

def process_channel(img: np.ndarray, channel_index: int, N: int, M: int) -> np.ndarray:
    """
    Generic function to process a specific channel in the image.

    Args:
        img (np.ndarray): The image to process.
        channel_index (int): The index of the channel to process.
        N (int): Height of the image.
        M (int): Width of the image.

    Returns:
        np.ndarray: The processed image.
    """

    for i in range(N):
        for j in range(M):
            if(img[i, j, channel_index] == 0):
                neighbors = find_neighbors(img, channel_index, i, j, N, M)
                direct_neighbors = find_direct_neighbors(neighbors)
                weights = find_weights(img, direct_neighbors, channel_index, i, j, N, M)
                img[i, j, channel_index] = interpolate(neighbors, weights)
    
    return img
                
