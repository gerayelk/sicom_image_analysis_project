"""A file containing a (pretty useless) reconstruction.
It serves as example of how the project works.
This file should NOT be modified.
"""


import numpy as np
from scipy.signal import convolve2d

from src.forward_model import CFA

# High quality linear interpolation filters for bayer mosaic
bayer_g_at_r = np.array([[ 0,  0, -1,  0,  0],
                         [ 0,  0,  2,  0,  0],
                         [-1,  2,  4,  2, -1],
                         [ 0,  0,  2,  0,  0],
                         [ 0,  0, -1,  0,  0]]) / 8
bayer_g_at_b = bayer_g_at_r
bayer_r_at_green_rrow_bcol = np.array([[ 0,  0, 1/2,  0,  0],
                                       [ 0, -1,  0,  -1,  0],
                                       [-1,  4,  5,   4, -1],
                                       [ 0, -1,  0,  -1,  0],
                                       [ 0,  0, 1/2,  0,  0]]) / 8
bayer_r_at_green_brow_rcol = bayer_r_at_green_rrow_bcol.T
bayer_b_at_green_rrow_bcol = bayer_r_at_green_brow_rcol
bayer_b_at_green_brow_rcol = bayer_r_at_green_rrow_bcol
bayer_r_at_b = np.array([[  0,  0, -3/2, 0,   0],
                            [  0,  2,   0,  2,   0],
                            [-3/2, 0,   6,  0, -3/2],
                            [  0,  2,   0,  2,   0],
                            [  0,  0, -3/2, 0,   0]]) / 8
bayer_b_at_r = bayer_r_at_b

# High quality linear interpolation filters for quad mosaic
quad_g_at_r = np.array([[ 0,  0,  0, 0, -1, -1, 0, 0,  0,  0],
                        [ 0,  0,  0, 0, -1, -1, 0, 0,  0,  0],
                        [ 0,  0,  0, 0,  2,  2, 0, 0,  0,  0],
                        [ 0,  0,  0, 0,  2,  2, 0, 0,  0,  0],
                        [-1, -1,  2, 2,  4,  4, 2, 2, -1, -1],
                        [-1, -1,  2, 2,  4,  4, 2, 2, -1, -1],
                        [ 0,  0,  0, 0,  2,  2, 0, 0,  0,  0],
                        [ 0,  0,  0, 0,  2,  2, 0, 0,  0,  0],
                        [ 0,  0,  0, 0, -1, -1, 0, 0,  0,  0],
                        [ 0,  0,  0, 0, -1, -1, 0, 0,  0,  0]]) / 32
quad_g_at_b = quad_g_at_r
quad_r_at_green_rrow_bcol = np.array([[ 0,  0,  0,  0, 1/2, 1/2,  0,  0,  0,  0],
                                      [ 0,  0,  0,  0, 1/2, 1/2,  0,  0,  0,  0],
                                      [ 0,  0, -1, -1,  0,   0,  -1, -1,  0,  0],
                                      [ 0,  0, -1, -1,  0,   0,  -1, -1,  0,  0],
                                      [-1, -1,  4,  4,  5,   5,   4,  4, -1, -1],
                                      [-1, -1,  4,  4,  5,   5,   4,  4, -1, -1],
                                      [ 0,  0, -1, -1,  0,   0,  -1, -1,  0,  0],
                                      [ 0,  0, -1, -1,  0,   0,  -1, -1,  0,  0],
                                      [ 0,  0,  0,  0, 1/2, 1/2,  0,  0,  0,  0],
                                      [ 0,  0,  0,  0, 1/2, 1/2,  0,  0,  0,  0]]) / 32
quad_r_at_green_brow_rcol = quad_r_at_green_rrow_bcol.T
quad_b_at_green_rrow_bcol = quad_r_at_green_brow_rcol
quad_b_at_green_brow_rcol = quad_r_at_green_rrow_bcol
quad_r_at_b = np.array([[  0,    0,  0,  0, -3/2, -3/2, 0,  0,   0,    0],
                        [  0,    0,  0,  0, -3/2, -3/2, 0,  0,   0,    0],
                        [  0,    0,  2,  2,   0,    0,  2,  2,   0,    0],
                        [  0,    0,  2,  2,   0,    0,  2,  2,   0,    0],
                        [-3/2, -3/2, 0,  0,   6,    6,  0,  0, -3/2, -3/2],
                        [-3/2, -3/2, 0,  0,   6,    6,  0,  0, -3/2, -3/2],
                        [  0,    0,  2,  2,   0,    0,  2,  2,   0,    0],
                        [  0,    0,  2,  2,   0,    0,  2,  2,   0,    0],
                        [  0,    0,  0,  0, -3/2, -3/2, 0,  0,   0,    0],
                        [  0,    0,  0,  0, -3/2, -3/2, 0,  0,   0,    0]]) / 32
quad_b_at_r = quad_r_at_b

def high_quality_linear_interpolation(op : CFA, y : np.ndarray) -> np.ndarray:
    z = op.adjoint(y)
    res = np.empty(op.input_shape)
    mask = op.mask
    R = 0 ; G = 1 ; B = 2

    if op.cfa == 'bayer':
        y_pad = np.pad(y, 2, 'constant', constant_values=0)

        for i in range(y.shape[0]):
            for j in range(y.shape[1]):
                i_pad = i+2 ; j_pad = j+2

                if mask[i, j, G] == 1: # We must estimate R and B components
                    res[i, j, G] = y[i, j]

                    # Estimate R (B at left and right or B at top and bottom)
                    if mask[i, max(0, j-1), R] == 1 or mask[i, min(y.shape[1]-1, j+1), R] == 1:
                        res[i, j, R] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_r_at_green_rrow_bcol, mode='valid'))
                    else:
                        res[i, j, R] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_r_at_green_brow_rcol, mode='valid'))

                    # Estimate B (R at left and right or R at top and bottom)
                    if mask[i, max(0, j-1), B] == 1 or mask[i, min(y.shape[1]-1, j+1), B] == 1:
                        res[i, j, B] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_b_at_green_brow_rcol, mode='valid'))
                    else:
                        res[i, j, B] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_b_at_green_rrow_bcol, mode='valid'))

                elif mask[i, j, R] == 1: # We must estimate G and B components
                    res[i, j, R] = y[i, j]
                    res[i, j, G] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_g_at_r, mode='valid'))
                    res[i, j, B] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_b_at_r, mode='valid'))

                else: # We must estimate R and G components
                    res[i, j, B] = y[i, j]
                    res[i, j, G] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_g_at_b, mode='valid'))
                    res[i, j, R] = float(convolve2d(y_pad[i_pad-2:i_pad+3, j_pad-2:j_pad+3], bayer_r_at_b, mode='valid'))

    else:
        y_pad = np.pad(y, 4, 'constant', constant_values=0)

        for i in range(0, y.shape[0], 2):
            for j in range(0, y.shape[1], 2):
                i_pad = i+4 ; j_pad = j+4

                if mask[i, j, G] == 1: # We must estimate R and B components
                    res[i:i+2, j:j+2, G] = y[i:i+2, j:j+2]

                    # Estimate R (B at left and right or B at top and bottom)
                    if mask[i, max(0, j-1), R] == 1 or mask[i, min(y.shape[1]-1, j+1), R] == 1:
                        res[i:i+2, j:j+2, R] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_r_at_green_rrow_bcol, mode='valid'))
                    else:
                        res[i:i+2, j:j+2, R] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_r_at_green_brow_rcol, mode='valid'))

                    # Estimate B (R at left and right or R at top and bottom)
                    if mask[i, max(0, j-1), B] == 1 or mask[i, min(y.shape[1]-1, j+1), B] == 1:
                        res[i:i+2, j:j+2, B] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_b_at_green_brow_rcol, mode='valid'))
                    else:
                        res[i:i+2, j:j+2, B] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_b_at_green_rrow_bcol, mode='valid'))

                elif mask[i, j, R] == 1: # We must estimate G and B components
                    res[i:i+2, j:j+2, R] = y[i:i+2, j:j+2]
                    res[i:i+2, j:j+2, G] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_g_at_r, mode='valid'))
                    res[i:i+2, j:j+2, B] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_b_at_r, mode='valid'))

                else: # We must estimate R and G components
                    res[i:i+2, j:j+2, B] = y[i:i+2, j:j+2]
                    res[i:i+2, j:j+2, G] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_g_at_b, mode='valid'))
                    res[i:i+2, j:j+2, R] = float(convolve2d(y_pad[i_pad-4:i_pad+6, j_pad-4:j_pad+6], quad_r_at_b, mode='valid'))

    return res

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
