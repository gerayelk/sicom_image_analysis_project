
import numpy as np

from src.forward_model import CFA
from src.methods.girardey.methods import HQ_interpolation


def run_reconstruction_HQ(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)

    res = HQ_interpolation(op, y)

    return res
